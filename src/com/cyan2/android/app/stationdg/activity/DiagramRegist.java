package com.cyan2.android.app.stationdg.activity;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import com.cyan2.android.app.stationdg.db.DBManager;
import com.cyan2.android.app.stationdg.db.dao.DiagramDao;
import com.cyan2.android.app.stationdg.db.dao.DiagramDetailDao;
import com.cyan2.android.app.stationdg.db.dao.LineDao;
import com.cyan2.android.app.stationdg.db.dao.StationDao;
import com.cyan2.android.app.stationdg.domain.Diagram;
import com.cyan2.android.app.stationdg.domain.DiagramDetail;
import com.cyan2.android.app.stationdg.domain.Line;
import com.cyan2.android.app.stationdg.domain.Station;
import com.cyan2.android.app.stationdg.parser.DiagramReader;
import com.cyan2.android.app.stationdg.service.DiagramParseService;
import com.cyan2.android.app.stationdg.util.NetworkUtils;

public class DiagramRegist extends Activity implements Runnable{
	
	private ProgressDialog progressDialog;
	
	private Station station;
	
	private Line line;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent intent = getIntent();
		station = (Station)intent.getSerializableExtra(Station.class.getName());
		line = (Line)intent.getSerializableExtra(Line.class.getName());
		
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("マイステーション登録中");
		progressDialog.setMessage("少し時間がかかります・・・");
		progressDialog.setIndeterminate(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			public void onCancel(DialogInterface dialog) {
				Intent intent = new Intent();
				intent.setClassName(getPackageName(),
						getClass().getPackage().getName() + ".MyStations");
				
				DiagramRegist.this.startActivity(intent);
				finish();
			}
		});
		
		progressDialog.show();
		
		Thread thread = new Thread(this);
		thread.start();
		
	}

	public void run() {
		
		Intent in = new Intent(this,DiagramParseService.class);
		startService(in);
		
//		regist();
		progressDialog.cancel();
	}
	
	private void regist(){
		//インターネットのチェック
		if(!NetworkUtils.isConnected(getApplicationContext())){
			new AlertDialog.Builder(DiagramRegist.this)
			.setTitle("ネットワークがみつかりません")
			.setMessage("ネットワークに接続してから再度実行してください")
			.setNegativeButton("キャンセル", null)
			.show();
			return;
		}

		//TODO キャンセルの処理
		
		
		DBManager dbManager = new DBManager(this);
		SQLiteDatabase db = dbManager.getWritableDatabase();
		Log.d("station-dg","db hash front:"+db.hashCode());

		DiagramReader r = new DiagramReader();
		r.setStationId(station.getStationCode());
		r.setLineId(line.getLineCode());

		try {
			
			Log.d("station-dg", "start transaction");
			dbManager.startTransaction();

			DiagramDetailDao ddDao = new DiagramDetailDao(db);
			ddDao.delete(station.getStationCode(), line.getLineCode());
			
			List<Diagram> diagrams = r.get();
			DiagramDao diagramDao = new DiagramDao(db);
			DiagramDetailDao detailDao = new DiagramDetailDao(db);
			
			Log.d("station-dg", "start insert diagram&details");
			for(Diagram d : diagrams){
				diagramDao.insert(d);
				for(DiagramDetail dd : d.getDiagramDetails()){
					dd.setDiagramId(d.getDiagramId());
					detailDao.insert(dd);
				}
			}
			Log.d("station-dg", "end insert diagram&details");

			Log.d("station-dg", "start insert station");
			StationDao stationDao = new StationDao(db);
			stationDao.insert(station);
			Log.d("station-dg", "end insert station");
			
			Log.d("station-dg", "start insert line");
			LineDao lineDao = new LineDao(db);
			lineDao.insert(line);
			Log.d("station-dg", "end insert line");
			
			Log.d("station-dg", "commit transaction");
			dbManager.commitTransaction();
		} catch (Exception e) {
			Log.e("station-dg", "regist error",e);
			//TODO トランザクション問題による緊急回避策
			new DiagramDetailDao(db).delete(station.getStationCode(), line.getLineCode());
		} finally{
			Log.d("station-dg", "end transaction");
			dbManager.endTransaction();
			dbManager.close();
		}
	}
}

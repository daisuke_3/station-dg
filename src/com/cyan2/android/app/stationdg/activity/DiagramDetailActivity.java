package com.cyan2.android.app.stationdg.activity;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.cyan2.android.app.stationdg.R;
import com.cyan2.android.app.stationdg.db.DBManager;
import com.cyan2.android.app.stationdg.domain.Diagram;
import com.cyan2.android.app.stationdg.domain.DiagramDetail;
import com.cyan2.android.app.stationdg.logic.DiagramDetailLogic;

public class DiagramDetailActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_diagram_details);

		Intent intent = getIntent();
		Diagram diagram = (Diagram)intent.getSerializableExtra(Diagram.class.getName());
		
		DBManager dbManager = new DBManager(this);
		
		List<DiagramDetail> details;
		try {
			details = new DiagramDetailLogic().find(dbManager,diagram,new Date(System.currentTimeMillis()),6);
		} finally {
			dbManager.close();
		}
		
		ArrayAdapter<DiagramDetail> adp = new ArrayAdapter<DiagramDetail>(this, android.R.layout.simple_list_item_1,details);
		
		ListView listView = (ListView)findViewById(R.id.diagram_detail_list_view);
		listView.setAdapter(adp);
	}
	
}

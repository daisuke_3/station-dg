package com.cyan2.android.app.stationdg.activity;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.cyan2.android.app.stationdg.R;
import com.cyan2.android.app.stationdg.domain.Station;
import com.cyan2.android.app.stationdg.parser.StationNameReader;
import com.cyan2.android.app.stationdg.util.NetworkUtils;

public class StationSearchByStationName extends Activity implements Runnable{
	
	private ProgressDialog progressDialog;

	private List<Station> stations;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_station_search_by_station_name);
		
		Button button = (Button)findViewById(R.id.Button01);
		button.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				
				progressDialog = new ProgressDialog(StationSearchByStationName.this);
				progressDialog.setTitle("取得中");
				progressDialog.setMessage("データ取得中・・・");
				progressDialog.setIndeterminate(false);
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
					public void onCancel(DialogInterface dialog) {
						if(stations == null || stations.isEmpty()){
							Toast.makeText(StationSearchByStationName.this, "検索結果がありません", Toast.LENGTH_LONG).show();
							return;
						}

						ArrayAdapter<Station> adapter = new ArrayAdapter<Station>(StationSearchByStationName.this, android.R.layout.simple_list_item_1,stations);
						
						ListView listView = (ListView)findViewById(R.id.station_search_result_list);
						listView.setAdapter(adapter);
						listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

							public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
								ListView listView = (ListView) parent;
								// クリックされたアイテムを取得します
								Station item = (Station) listView.getItemAtPosition(position);
								
								Intent intent = new Intent();
								intent.setClassName(getPackageName(),
										getClass().getPackage().getName() + ".LineSearch");
								intent.putExtra(Station.class.getName(), item);
								
								StationSearchByStationName.this.startActivity(intent);
							}
						});
					}
				});
				progressDialog.show();

				Thread thread = new Thread(StationSearchByStationName.this);
				thread.start();
			}
		});
	}
	
	public void run() {
		getDiagram();
		progressDialog.cancel();
	}

	private void getDiagram(){
		if(!NetworkUtils.isConnected(getApplicationContext())){
			new AlertDialog.Builder(StationSearchByStationName.this)
			.setTitle("ネットワークがみつかりません")
			.setMessage("ネットワークに接続してから再度実行してください")
			.setNegativeButton("キャンセル", null)
			.show();
			return;
		}
		
		StationNameReader r = new StationNameReader();
		EditText et = (EditText)findViewById(R.id.EditText01);
		r.setSearchName(et.getText().toString());
		try {
			stations =  r.get();
		} catch (Exception e) {
			Log.w("diagram-dg", "error",e);
		}
	}
}

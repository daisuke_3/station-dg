package com.cyan2.android.app.stationdg.activity;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.cyan2.android.app.stationdg.R;
import com.cyan2.android.app.stationdg.db.DBManager;
import com.cyan2.android.app.stationdg.db.dao.DiagramDao;
import com.cyan2.android.app.stationdg.db.dao.DiagramDetailDao;
import com.cyan2.android.app.stationdg.domain.Diagram;
import com.cyan2.android.app.stationdg.service.DiagramParseService;

public class MyStations extends Activity implements Runnable{
	
	private static final int MENU_ADD = 0;
	private static final int DIALOG_ADD = 10;

	private ProgressDialog progressDialog;

	private List<Diagram> myDiagrams;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		int id = getIntent().getIntExtra(DiagramParseService.NOTIFICATION_ID_KEY, -1);
		Log.d("station-dg", "notification id :" + id);
//		if(id != -1){
//			NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//			manager.cancel(id);
//		}
		
		setContentView(R.layout.layout_my_stations);
		
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("取得中");
		progressDialog.setMessage("データ取得中・・・");
		progressDialog.setIndeterminate(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			public void onCancel(DialogInterface dialog) {
				if(myDiagrams.isEmpty()){
					Toast.makeText(MyStations.this, "マイステーションが登録されていません", Toast.LENGTH_LONG).show();
					return;
				}
				createListView();
			}
		});
		
		progressDialog.show();
		
		Thread thread = new Thread(this);
		thread.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		
		MenuItem menuItem = menu.add(0, MENU_ADD, 0, "マイステーション追加");
		
		menuItem.setIcon(android.R.drawable.ic_input_add);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if(item.getItemId() == MENU_ADD){
			showDialog(DIALOG_ADD);
		}
		
		return true;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		
		if(DIALOG_ADD == id){
			return new AlertDialog.Builder(this)
			.setTitle("検索方法の決定")
			.setItems(new String[]{"駅名から検索","路線から検索","都道府県から検索"}, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					Log.i("search way", "" + which);
					switch(which){
					case 0:
						Intent intent = new Intent();
						intent.setClassName(getPackageName(),
								getClass().getPackage().getName() + ".StationSearchByStationName");
						
						MyStations.this.startActivity(intent);
						finish();
						break;
					case 1:
						Toast.makeText(MyStations.this, "まだ実装されていません", Toast.LENGTH_LONG).show();
						break;
					case 2:
						Toast.makeText(MyStations.this, "まだ実装されていません", Toast.LENGTH_LONG).show();
						break;
					}
				}
			}).create();
		}
		return null;
	}

	public void run() {
		getMyDiagrams();
		progressDialog.cancel();
	}
	
	private void createListView(){
		ArrayAdapter<Diagram> adapter = new ArrayAdapter<Diagram>(MyStations.this, android.R.layout.simple_list_item_1,myDiagrams);
		
		ListView listView = (ListView)findViewById(R.id.my_stations_list_view);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

			public void onItemClick(AdapterView<?> parent, View arg1,
					int position, long arg3) {
				ListView listView = (ListView) parent;
				// クリックされたアイテムを取得します
				Diagram item = (Diagram) listView.getItemAtPosition(position);
				
				Intent intent = new Intent();
				intent.setClassName(getPackageName(),
						getClass().getPackage().getName() + ".DiagramDetailActivity");
				intent.putExtra(Diagram.class.getName(), item);
				
				MyStations.this.startActivity(intent);
				
			}
			
		});
		
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent,
					View arg1, int position, long arg3) {
				
				ListView listView = (ListView) parent;
				// クリックされたアイテムを取得します
				final Diagram item = (Diagram) listView.getItemAtPosition(position);
				
				
				new AlertDialog.Builder(MyStations.this)
				.setTitle("削除確認")
				.setMessage("選択した時刻表を削除します。よろしいですか？")
				.setPositiveButton("はい", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Log.d("station-dg", "clicked delete button");
						DBManager dbManager = new DBManager(MyStations.this);
						SQLiteDatabase db = dbManager.getReadableDatabase();
						try {
							DiagramDao dDao = new DiagramDao(db);
							DiagramDetailDao ddDao = new DiagramDetailDao(db);
							dbManager.startTransaction();
							dDao.delete(item.getStationCode(),item.getLineCode());
							ddDao.delete(item.getStationCode(), item.getLineCode());
							dbManager.commitTransaction();
						} finally {
							dbManager.endTransaction();
							dbManager.close();
						}
						getMyDiagrams();
						createListView();
					}
				})
				.setNegativeButton("いいえ", null)
				.show();
				
				
				return false;
			}
			
		});
	}
	
	private void getMyDiagrams(){
		Log.d("station-dg", "search my diagrams");
		DBManager dbManager = new DBManager(this);
		SQLiteDatabase db = dbManager.getReadableDatabase();
		
		try {
			DiagramDao dao = new DiagramDao(db);
			myDiagrams = dao.findAll();
		} finally {
			dbManager.close();
		}
	}
}

package com.cyan2.android.app.stationdg.domain;

import java.util.Calendar;
import java.util.Date;

public enum DayType {
	WEEK_DAY("����","0"),
	SATURDAY("�y�j","1"),
	HOLIDAY("���j/�j��","2");
	
	private String string;
	private String code;
	private DayType(String string,String code) {
		this.string = string;
		this.code = code;
	}
	public String getString() {
		return string;
	}
	
	public String getCode() {
		return code;
	}
	public static DayType get(String string){
		for(DayType dt : values()){
			if(string.contains(dt.getString())){
				return dt;
			}
		}
		return null;
	}
	
	public static DayType getDayType(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dow = c.get(Calendar.DAY_OF_WEEK);
		
		switch (dow) {
		case Calendar.SUNDAY:
			return DayType.HOLIDAY;
		case Calendar.SATURDAY:
			return DayType.SATURDAY;
		default:
			return DayType.WEEK_DAY;
		}
	}
}

package com.cyan2.android.app.stationdg.domain;

import java.io.Serializable;
import java.util.List;

public class Diagram implements Serializable{
	private static final long serialVersionUID = 1L;

	private int diagramId;
	
	private String stationCode;
	
	private String stationName;
		
	private String lineCode;
	
	private String lineName;

	private DayType dayType;
	
	private String districtName;
	
	private List<DiagramDetail> diagramDetails;
	
	public int getDiagramId() {
		return diagramId;
	}

	public void setDiagramId(int diagramId) {
		this.diagramId = diagramId;
	}

	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}
	

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getLineCode() {
		return lineCode;
	}

	public void setLineCode(String lineCode) {
		this.lineCode = lineCode;
	}
	

	public String getLineName() {
		return lineName;
	}

	public void setLineName(String lineName) {
		this.lineName = lineName;
	}

	public DayType getDayType() {
		return dayType;
	}

	public void setDayType(DayType dayType) {
		this.dayType = dayType;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public List<DiagramDetail> getDiagramDetails() {
		return diagramDetails;
	}

	public void setDiagramDetails(List<DiagramDetail> diagramDetails) {
		this.diagramDetails = diagramDetails;
	}

	@Override
	public String toString() {
		return stationName + ":" + lineName + ":" + districtName;
	}
}

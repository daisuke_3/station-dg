package com.cyan2.android.app.stationdg.widget;

import java.util.ArrayList;
import java.util.List;

import com.cyan2.android.app.stationdg.R;
import com.cyan2.android.app.stationdg.db.DBManager;
import com.cyan2.android.app.stationdg.db.dao.DiagramDao;
import com.cyan2.android.app.stationdg.domain.Diagram;

import android.app.Activity;
import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

public class StationDGAppWidgetConfigure extends Activity {

	private static final String PREFS_NAME = "com.cyan2.android.app.stationdg.widget.StationDGWidgetProvider";
	private static final String PREF_PREFIX_KEY_STATION_CODE = "station_code_";
	private static final String PREF_PREFIX_KEY_STATION_NAME = "station_name_";
	private static final String PREF_PREFIX_KEY_LINE_CODE = "line_code_";
	private static final String PREF_PREFIX_KEY_LINE_NAME = "line_name_";
	private static final String PREF_PREFIX_KEY_DISTRICT_NAME = "district_name_";

	private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
	
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		// Set the result to CANCELED.  This will cause the widget host to cancel
		// out of the widget placement if they press the back button.
		setResult(RESULT_CANCELED);

		// Set the view layout resource to use.
		setContentView(R.layout.app_widget_configure);
		
		// Find the widget id from the intent. 
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
			mAppWidgetId = extras.getInt(
					AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		}

		// If they gave us an intent without the widget id, just bail.
		if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
			finish();
		}
		
		DBManager dbManager = new DBManager(this);
		
		try {
			final List<Diagram> diagrams = new DiagramDao(dbManager.getReadableDatabase()).findAll();
			
			new AlertDialog.Builder(this)
			.setTitle("表示する時刻表を選択してください")
			.setItems(convertToViewList(diagrams), new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					final Context context = StationDGAppWidgetConfigure.this;

					// When the button is clicked, save the string in our prefs and return that they
					// clicked OK.
					Diagram d = diagrams.get(which);
					saveTitlePref(context, mAppWidgetId, d);

					// Push widget update to surface with newly set prefix
					AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
					StationDGWidgetProvider.updateAppWidget(context, appWidgetManager,mAppWidgetId,d);

					// Make sure we pass back the original appWidgetId
					Intent resultValue = new Intent();
					resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
					setResult(RESULT_OK, resultValue);
					finish();
				}
				
			}).show();
		} finally {
			dbManager.close();
		}

	}
	
	private String[] convertToViewList(List<Diagram> diagrams){
		List<String> strings = new ArrayList<String>();
		for(Diagram d : diagrams ){
			strings.add(d.toString());
		}
		return strings.toArray(new String[strings.size()]);
	}

	// Write the prefix to the SharedPreferences object for this widget
	static void saveTitlePref(Context context, int appWidgetId, Diagram diagram) {
		SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
		prefs.putString(PREF_PREFIX_KEY_STATION_CODE + appWidgetId, diagram.getStationCode());
		prefs.putString(PREF_PREFIX_KEY_STATION_NAME + appWidgetId, diagram.getStationName());
		prefs.putString(PREF_PREFIX_KEY_LINE_CODE + appWidgetId, diagram.getLineCode());
		prefs.putString(PREF_PREFIX_KEY_LINE_NAME + appWidgetId, diagram.getLineName());
		prefs.putString(PREF_PREFIX_KEY_DISTRICT_NAME + appWidgetId, diagram.getDistrictName());
		prefs.commit();
	}
	
	static void deleteTitlePref(Context context, int appWidgetId) {
		SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
		prefs.remove(PREF_PREFIX_KEY_STATION_CODE + appWidgetId);
		prefs.remove(PREF_PREFIX_KEY_STATION_NAME + appWidgetId);
		prefs.remove(PREF_PREFIX_KEY_LINE_CODE + appWidgetId);
		prefs.remove(PREF_PREFIX_KEY_LINE_NAME + appWidgetId);
		prefs.remove(PREF_PREFIX_KEY_DISTRICT_NAME + appWidgetId);
		prefs.commit();
	}

	// Read the prefix from the SharedPreferences object for this widget.
	// If there is no preference saved, get the default from a resource
	static Diagram loadTitlePref(Context context, int appWidgetId) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
		String stationCode = prefs.getString(PREF_PREFIX_KEY_STATION_CODE + appWidgetId, null);
		String stationName = prefs.getString(PREF_PREFIX_KEY_STATION_NAME + appWidgetId, null);
		String lineCode = prefs.getString(PREF_PREFIX_KEY_LINE_CODE + appWidgetId, null);
		String lineName = prefs.getString(PREF_PREFIX_KEY_LINE_NAME + appWidgetId, null);
		String districtName = prefs.getString(PREF_PREFIX_KEY_DISTRICT_NAME + appWidgetId, null);
		
		if(stationCode == null || stationName == null || lineCode == null || lineName == null || districtName == null){
			return null;
		}
		
		Diagram d = new Diagram();
		d.setStationCode(stationCode);
		d.setStationName(stationName);
		d.setLineCode(lineCode);
		d.setLineName(lineName);
		d.setDistrictName(districtName);

		return d;
	}
}

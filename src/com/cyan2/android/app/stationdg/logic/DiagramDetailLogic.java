package com.cyan2.android.app.stationdg.logic;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan2.android.app.stationdg.db.DBManager;
import com.cyan2.android.app.stationdg.db.dao.DiagramDao;
import com.cyan2.android.app.stationdg.db.dao.DiagramDetailDao;
import com.cyan2.android.app.stationdg.domain.DayType;
import com.cyan2.android.app.stationdg.domain.Diagram;
import com.cyan2.android.app.stationdg.domain.DiagramDetail;

public class DiagramDetailLogic {
	
	public List<DiagramDetail> find(DBManager dbManager,Diagram diagram,Date date,int limit){
		SQLiteDatabase db = dbManager.getReadableDatabase();
		Log.d("station-dg","db hash:"+db.hashCode());

		
		DiagramDao diagramDao = new DiagramDao(db);
		
		Time now = new Time(System.currentTimeMillis());
		
		DayType dayType = DayType.getDayType(date);
		int id = diagramDao.findId(diagram.getStationCode(), diagram.getLineCode(), diagram.getDistrictName(), dayType);
		if(id == -1){
			return new ArrayList<DiagramDetail>();
		}
		
		
		DiagramDetailDao detailDao = new DiagramDetailDao(db);
		
		List<DiagramDetail> list = detailDao.find(id, now, limit);
		if(list.size() == limit){
			return list;
		}
		int sa = limit - list.size();
		int upperId;
		if(list.size() > 0){
			upperId = list.get(list.size()-1).getDiagramDetailId();
		}else{
			upperId = detailDao.getMaxId(id);
		}
		list.addAll(detailDao.findUpperDiagramId(id,upperId ,"00:00", sa));
		if(list.size() == limit){
			return list;
		}
		sa = limit - list.size();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1);
		
		dayType = DayType.getDayType(cal.getTime());
		id = diagramDao.findId(diagram.getStationCode(), diagram.getLineCode(), diagram.getDistrictName(), dayType);
		
		list.addAll(detailDao.find(id, sa));
		
		return list;
	}
	
	public boolean isFinal(DBManager dbManager, DiagramDetail diagramDetail){
		SQLiteDatabase db = dbManager.getReadableDatabase();

		DiagramDetailDao detailDao = new DiagramDetailDao(db);
		
		int id = detailDao.getMaxId(diagramDetail.getDiagramId());
		if(id == diagramDetail.getDiagramDetailId()){
			return true;
		}
		return false;
	}
	
	public boolean isFirst(DBManager dbManager, DiagramDetail diagramDetail){
		SQLiteDatabase db = dbManager.getReadableDatabase();

		DiagramDetailDao detailDao = new DiagramDetailDao(db);
		
		int id = detailDao.getMinId(diagramDetail.getDiagramId());
		if(id == diagramDetail.getDiagramDetailId()){
			return true;
		}
		return false;
	}
}

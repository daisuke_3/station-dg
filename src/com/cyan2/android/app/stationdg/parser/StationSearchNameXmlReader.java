package com.cyan2.android.app.stationdg.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLFilterImpl;

import com.cyan2.android.app.stationdg.domain.Station;

public class StationSearchNameXmlReader extends XMLFilterImpl{
	
	private static final String SCRIPT_SEARCH_STRING = "sList\\['%s'\\] = \\{name:'(.+)', code:'(.+)'\\};";
	
	private int stationDivCount;
	
	private boolean isScript;
	
	private StringBuilder script;
	
	private List<String> stationInternalIds;
	
	private List<Station> stations;
	
	public StationSearchNameXmlReader() {
		stationInternalIds = new ArrayList<String>();
		stations = new ArrayList<Station>();
		script = new StringBuilder();
	}
	
	public List<Station> getStations() {
		return stations;
	}

	@Override
	public void characters(char[] chars, int start, int length) throws SAXException {
		if(isScript){
			script.append(SAXUtil.toString(chars, start, length));
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		if(SAXUtil.TAG_NAME_SCRIPT.equals(qName)){
			
			for(String internalId : this.stationInternalIds){
				Pattern p = Pattern.compile(String.format(SCRIPT_SEARCH_STRING, internalId));
				Matcher m = p.matcher(script.toString());
				if(m.find()){
					Station station = new Station();
					station.setStationCode(m.group(2));
					station.setStationName(m.group(1));
					
					stations.add(station);
				}
			}
			isScript = false;
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attr) throws SAXException {
		
		if(SAXUtil.TAG_NAME_DIV.equals(qName) && SAXUtil.containsAttribute(attr, SAXUtil.ATTR_NAME_CLASS, "dia_cate_label")){
			stationDivCount++;
			return;
		}
		
		if(stationDivCount == 1 && SAXUtil.TAG_NAME_DIV.equals(qName)){
			String id = SAXUtil.getAttributeValue(attr, SAXUtil.ATTR_NAME_ID);
			if(id != null){
				stationInternalIds.add(id);
			}
		}
		
		if(SAXUtil.TAG_NAME_SCRIPT.equals(qName)){
			isScript = true;
		}
	}
}

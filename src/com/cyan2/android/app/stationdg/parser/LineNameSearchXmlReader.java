package com.cyan2.android.app.stationdg.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLFilterImpl;

import com.cyan2.android.app.stationdg.domain.Line;

public class LineNameSearchXmlReader extends XMLFilterImpl {
	private static final String SCRIPT_SEARCH_STRING = "rList\\['%s'\\] = \\{code:'(.+)'\\};";
	
	private List<String> internalIds;
	
	private boolean isScript;
	
	private String internalId;
	
	private boolean isLineDiv;
	
	private StringBuilder script;
	
	private List<Line> lines;
	
	public LineNameSearchXmlReader() {
		internalIds = new ArrayList<String>();
		lines = new ArrayList<Line>();
		script = new StringBuilder();
	}

	public List<Line> getLines() {
		return lines;
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		
		if(isLineDiv && internalId != null){
			String name = SAXUtil.toString(ch, start, length);
			internalIds.add(internalId + ":" + name);
			internalId = null;
			isLineDiv = false;
		}
		
		if(isScript){
			script.append(SAXUtil.toString(ch, start, length));
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if(isScript){
			
			for(String internalId : internalIds){
				String[] s = internalId.split(":");
				
				Pattern p = Pattern.compile(String.format(SCRIPT_SEARCH_STRING, s[0]));
				Matcher m = p.matcher(script.toString());
				if(m.find()){
					String id = m.group(1);
					
					Line line = new Line();
					line.setLineCode(id);
					line.setLineName(s[1]);
					
					lines.add(line);
				}
			}
			
			isScript = false;
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {

		if(SAXUtil.TAG_NAME_DIV.equals(qName)){
			String id = SAXUtil.getAttributeValue(atts, SAXUtil.ATTR_NAME_ID);
			if(id != null && id.startsWith("railRoad_")){
				isLineDiv = true;
				internalId = id;
			}
		}
		
		if(SAXUtil.TAG_NAME_SCRIPT.equals(qName)){
			isScript = true;
		}
	}
}

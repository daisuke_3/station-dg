package com.cyan2.android.app.stationdg.parser;

import static com.cyan2.android.app.stationdg.parser.SAXUtil.*;

import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLFilterImpl;

import android.util.Log;

import com.cyan2.android.app.stationdg.domain.DayType;
import com.cyan2.android.app.stationdg.domain.Diagram;
import com.cyan2.android.app.stationdg.domain.DiagramDetail;


public class DiagramXmlReader extends XMLFilterImpl{
	
	private static final String ATTR_VALUE_TC = "tc";
	private static final String ATTR_VALUE_G = "g";
	private static final String ATTR_VALUE_E = "e";
	private static final String ATTR_VALUE_M = "m";
	private static final String ATTR_VALUE_S1 = "s1";
	private static final String ATTR_VALUE_S2 = "s2";
	private static final String ATTR_VALUE_S3 = "s3";
	private static final String ATTR_VALUE_T_T1 = "t t1";
	private static final String ATTR_VALUE_T_T2 = "t t2";
	private static final String ATTR_VALUE_T_T3 = "t t3";
	
	private static final String DEFAULT_KIND_CODE = "d21";
	private static final String DEFAULT_KIND_NAME = "各駅停車";

	private static final String DEFAULT_DESTINATION_NAME = "無印";
	
	private boolean diagramHeader;
	
	private boolean isKindBlockDiv;
	
	private boolean isKindBlockUl;

	private boolean isKindBlockLi;
	
	private boolean isDestinationDiv;
	private boolean isDestinationUl;
	private boolean isDestinationLi;
	
	private boolean hourDiagramDiv;
	private boolean hourDiagramHour;
	private boolean hourDiagramMinuteDiv;
	private boolean hourDiagramMinute;
	
	private String kindKey;
	
	private String hour;
	
	private List<Diagram> diagrams;
	
	private Diagram diagram;
	
	private DiagramDetail diagramDetail;
	
	private Map<String, String> trainKinds;
	private Map<String, String> destinations;
	
	private String stationCode;
	private String lineCode;
	
	public DiagramXmlReader() {
		diagrams = new ArrayList<Diagram>();
		trainKinds = new HashMap<String, String>();
		destinations = new HashMap<String, String>();
	}
	
	public List<Diagram> getDiagrams() {
		return diagrams;
	}
	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public String getLineCode() {
		return lineCode;
	}

	public void setLineCode(String lineCode) {
		this.lineCode = lineCode;
	}

	private void optimize(){
		//ここで行き先、種別をセット
		for(DiagramDetail dd : diagram.getDiagramDetails()){
			String kindId = dd.getKindId();
			String kind;
			if(DEFAULT_KIND_CODE.equals(kindId)){
				kind = DEFAULT_KIND_NAME;
			}else{
				kind = trainKinds.get(kindId);
			}
			
			dd.setKindName(kind);
			
			String destination = destinations.get(dd.getDestination());
			if(destination == null){
				destination = BLANK;
			}
			
			dd.setDestination(destination);
		}
	}

	@Override
	public void characters(char[] chars, int start, int length) throws SAXException {
		
		if(diagramHeader){
			String body = SAXUtil.toString(chars, start, length);
			body = body.replaceAll(DOUBLE_SPACE, SPACE);
			String[] ss = body.split(SPACE);
			
			diagram.setDayType(DayType.get(ss[2]));
			diagram.setDistrictName(ss[1]);
		}
		
		if(isKindBlockLi){
			trainKinds.put(kindKey, SAXUtil.toString(chars, start, length));
		}
		
		if(isDestinationLi){
			String content = SAXUtil.toString(chars, start, length);
			String[] ss = content.split(EQUAL);
			if(ss[0].equals(DEFAULT_DESTINATION_NAME)){
				ss[0] = "";
			}
			destinations.put(ss[0], ss[1]);
		}
		if(hourDiagramHour){
			hour = SAXUtil.toString(chars, start, length);
		}
		if(hourDiagramMinute){
			String tc = SAXUtil.toString(chars, start, length);
			tc = tc.replaceAll(REGEX_RETURN, BLANK);
			tc = tc.replaceAll(REGEX_TAB, BLANK);
			
			if(diagramDetail.getDepartureTime() == null){
				diagramDetail.setDepartureTime(hour + ":" + tc);
			}else{
				//この時はまだ略称
				diagramDetail.setDestination(tc);
			}
		}
	}

	@Override
	public void endElement(String arg0, String arg1, String arg2)
			throws SAXException {
		
		//１時間分の時刻表
		if(TAG_NAME_A.equals(arg2) && hourDiagramMinuteDiv){
			hourDiagramMinute = false;
		}
		if(TAG_NAME_DIV.equals(arg2) && hourDiagramDiv){
			hourDiagramMinuteDiv = false;
		}
		if(TAG_NAME_P.equals(arg2) && hourDiagramDiv){
			hourDiagramHour = false;
		}
		if(TAG_NAME_DIV.equals(arg2) && hourDiagramDiv){
			hourDiagramDiv = false;
			hour = null;
		}
		
		
		//いきさき
		if(TAG_NAME_LI.equals(arg2) && isDestinationLi){
			isDestinationLi = false;
		}
		
		if(TAG_NAME_UL.equals(arg2) && isDestinationUl){
			isDestinationUl = false;
		}
		if(TAG_NAME_DIV.equals(arg2) && isDestinationDiv){
			isDestinationDiv = false;
		}
		
		
		if(TAG_NAME_LI.equals(arg2) && isKindBlockLi){
			isKindBlockLi = false;
			kindKey = null;
		}
		
		if(TAG_NAME_UL.equals(arg2) && isKindBlockUl){
			isKindBlockUl = false;
		}
		
		if(TAG_NAME_H2.equals(arg2) && diagramHeader){
			diagramHeader = false;
		}
		
		//列車種別のブロック
		if(TAG_NAME_DIV.equals(arg2) && isKindBlockDiv){
			isKindBlockDiv = false;
		}

		//時刻表のブロックかどうか
	}

	@Override
	public void endDocument() throws SAXException {
		if(diagram != null){
			optimize();
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attr) throws SAXException {
		
		//時刻表のブロックかどうか
		if(TAG_NAME_DIV.equals(qName) && containsAttribute(attr, ATTR_NAME_CLASS, ATTR_VALUE_M)){
			if(diagram != null){
				optimize();
			}
			diagram = new Diagram();
			diagram.setDiagramDetails(new ArrayList<DiagramDetail>());
			diagram.setStationCode(stationCode);
			diagram.setLineCode(lineCode);
			
			diagrams.add(diagram);
			
		}
		
		
		if(TAG_NAME_H2.equals(qName) && (containsAttribute(attr, ATTR_NAME_CLASS, ATTR_VALUE_S1)
				||containsAttribute(attr, ATTR_NAME_CLASS, ATTR_VALUE_S2)
				||containsAttribute(attr, ATTR_NAME_CLASS, ATTR_VALUE_S3))){
			diagramHeader = true;
		}
		
		//列車種別のブロック
		if(TAG_NAME_DIV.equals(qName) && containsAttribute(attr, ATTR_NAME_CLASS, ATTR_VALUE_E)){
			isKindBlockDiv = true;
			trainKinds.clear();
		}
		
		if(TAG_NAME_UL.equals(qName) && isKindBlockDiv){
			isKindBlockUl = true;
		}
		if(TAG_NAME_LI.equals(qName) && isKindBlockUl){
			isKindBlockLi = true;
			kindKey = getAttributeValue(attr, ATTR_NAME_CLASS);
		}
		
		
		//行き先のブロック
		if(TAG_NAME_DIV.equals(qName) && containsAttribute(attr, ATTR_NAME_CLASS, ATTR_VALUE_G)){
			isDestinationDiv = true;
			destinations.clear();
		}
		if(TAG_NAME_UL.equals(qName) && isDestinationDiv){
			isDestinationUl = true;
		}
		if(TAG_NAME_LI.equals(qName) && isDestinationUl){
			isDestinationLi = true;
		}
		
		//１時間分の時刻表
		if(TAG_NAME_DIV.equals(qName) && (containsAttribute(attr, ATTR_NAME_CLASS, ATTR_VALUE_T_T1)
				|| containsAttribute(attr, ATTR_NAME_CLASS, ATTR_VALUE_T_T2)
				|| containsAttribute(attr, ATTR_NAME_CLASS, ATTR_VALUE_T_T3))){
			hourDiagramDiv = true;
		}
		if(TAG_NAME_P.equals(qName) && hourDiagramDiv){
			hourDiagramHour = true;
		}
		if(TAG_NAME_DIV.equals(qName) && hourDiagramDiv && containsAttribute(attr, ATTR_NAME_CLASS, ATTR_VALUE_TC)){
			hourDiagramMinuteDiv = true;
		}
		if(TAG_NAME_A.equals(qName) && hourDiagramMinuteDiv){
			hourDiagramMinute = true;
			
			diagramDetail = new DiagramDetail();
			diagramDetail.setKindId(getAttributeValue(attr, ATTR_NAME_CLASS));
			diagram.getDiagramDetails().add(diagramDetail);
		}
		
	}
}

package com.cyan2.android.app.stationdg.parser;

import java.io.StringReader;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.ccil.cowan.tagsoup.HTMLSchema;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.cyan2.android.app.stationdg.domain.Line;


public class LineNameSearchReader {
	private static final String SEARCH_URL = "http://www.navitime.co.jp/pcnavi/nd0121020.jsp?stationCode=%s&pageInd=0";

	private String stationCode;

	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public List<Line> get()throws Exception{
		DefaultHttpClient client = new DefaultHttpClient();
		
		String url = String.format(SEARCH_URL, URLEncoder.encode(stationCode, "UTF-8"));
		HttpGet httpget = new HttpGet(url);
				
		HttpResponse response = client.execute(httpget);
		String html = EntityUtils.toString(response.getEntity());
		html = html.replaceAll("&nbsp;", " ");
		html = html.replaceAll("&nbsp", " ");
		
		XMLReader parser = new Parser();
		
		HTMLSchema schema = new HTMLSchema();
		parser.setProperty(Parser.schemaProperty, schema);
		
		LineNameSearchXmlReader serializer = new LineNameSearchXmlReader();
		
		parser.setContentHandler(serializer);
		// 属性へのデフォルト付与を抑制させます。
        parser.setFeature(Parser.defaultAttributesFeature, false);
        
		InputSource input = new InputSource();
		input.setCharacterStream(new StringReader(html));

		parser.parse(input);
		
		return serializer.getLines();
	}
	
	public static void main(String[] args) throws Exception {
		
		LineNameSearchReader reader = new LineNameSearchReader();
		reader.setStationCode("00004464");
		
		System.out.println(new Timestamp(System.currentTimeMillis()));
		List<Line> lines = reader.get();
		System.out.println(new Timestamp(System.currentTimeMillis()));
		for(Line s : lines){
			System.out.println(s.getLineCode() + ":" + s.getLineName());
		}
	}
}

package com.cyan2.android.app.stationdg.parser;

import java.io.StringReader;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.ccil.cowan.tagsoup.HTMLSchema;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan2.android.app.stationdg.db.dao.DiagramDao;
import com.cyan2.android.app.stationdg.db.dao.DiagramDetailDao;
import com.cyan2.android.app.stationdg.domain.Diagram;
import com.cyan2.android.app.stationdg.domain.DiagramDetail;


public class DiagramReader{
	
	private static final String SEARCH_URL = "http://www.navitime.co.jp/diagram/";
	
	private String stationCode;
	
	private String lineCode;
		
	private DiagramDao diagramDao;
	
	private DiagramDetailDao diagramDetailDao;
	
	public List<Diagram> get()throws Exception{

		DefaultHttpClient client = new DefaultHttpClient();
		
		HttpGet httpget = new HttpGet(SEARCH_URL + stationCode + "_" + lineCode); 
		
		Log.d("station-dg", "start internet");
		HttpResponse response = client.execute(httpget);
		Log.d("station-dg", "end internet");
		
		Log.d("station-dg", "start replace");
		String html = EntityUtils.toString(response.getEntity(),"UTF-8");
		html = html.replaceAll("&nbsp;", " ");
		html = html.replaceAll("&nbsp", " ");
		Log.d("station-dg", "end replace");
		
		XMLReader parser = new Parser();
		
		HTMLSchema schema = new HTMLSchema();
		parser.setProperty(Parser.schemaProperty, schema);
		
		DiagramXmlReader serializer = new DiagramXmlReader();
		serializer.setStationCode(stationCode);
		serializer.setLineCode(lineCode);
		
		parser.setContentHandler(serializer);
		// 属性へのデフォルト付与を抑制させます。
        parser.setFeature(Parser.defaultAttributesFeature, false);
        
		InputSource input = new InputSource();
		input.setCharacterStream(new StringReader(html));

		Log.d("station-dg", "start parse");
		parser.parse(input);
		Log.d("station-dg", "end parse");
		
		return serializer.getDiagrams();
	}
	
	public void addDiagram(Diagram diagram) {
		Log.d("station-dg", "start insert diagrams");
		diagramDao.insert(diagram);
		Log.d("station-dg", "end insert diagrams");
	}

	public void addDiagramDetail(DiagramDetail diagramDetail) {
		Log.d("station-dg", "start insert diagram-details");
		diagramDetailDao.insert(diagramDetail);
		Log.d("station-dg", "end insert diagram-details");
	}

	public String getStationId() {
		return stationCode;
	}

	public void setStationId(String stationId) {
		this.stationCode = stationId;
	}

	public String getLineId() {
		return lineCode;
	}

	public void setLineId(String lineId) {
		this.lineCode = lineId;
	}
}

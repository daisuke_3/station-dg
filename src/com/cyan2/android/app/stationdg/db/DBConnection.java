package com.cyan2.android.app.stationdg.db;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConnection extends SQLiteOpenHelper {

	private static DBConnection instance;
	
	private static final String DATABASE_NAME = "station-dg-4.db";
	private static final int DATABASE_VERSION = 1;
	
	
	private DBConnection(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	public static DBConnection getInstance(Context application){
		if(instance == null){
			instance = new DBConnection(application);
		}
		return instance;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE diagrams (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , station_code VARCHAR NOT NULL , line_code VARCHAR NOT NULL , district_name VARCHAR NOT NULL , day_type VARCHAR NOT NULL , regist_date DATETIME NOT NULL  DEFAULT CURRENT_TIMESTAMP)");
		db.execSQL("CREATE TABLE diagram_details (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , diagram_id INTEGER NOT NULL , departure_time DATETIME NOT NULL , kind_id VARCHAR NOT NULL , kind_name VARCHAR NOT NULL , destination VARCHAR NOT NULL , regist_date DATETIME NOT NULL  DEFAULT CURRENT_TIMESTAMP)");
		db.execSQL("CREATE TABLE lines (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , line_code VARCHAR NOT NULL  UNIQUE , line_name VARCHAR NOT NULL , regist_date DATETIME NOT NULL  DEFAULT CURRENT_TIMESTAMP)");
		db.execSQL("CREATE TABLE stations (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , station_code VARCHAR NOT NULL  UNIQUE , station_name VARCHAR NOT NULL , regist_date DATETIME NOT NULL  DEFAULT CURRENT_TIMESTAMP)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}

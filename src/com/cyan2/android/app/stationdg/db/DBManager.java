package com.cyan2.android.app.stationdg.db;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DBManager {
	
	private DBConnection connection;
	
	public DBManager(Context context) {
		this.connection = DBConnection.getInstance(context);
	}
	
	public void startTransaction(){
		connection.getWritableDatabase().beginTransaction();
//		if(writable == null || !writable.isOpen()){
//			writable = connection.getWritableDatabase();
//		}
//		writable.beginTransaction();
	}
	
	public void commitTransaction(){
		connection.getWritableDatabase().setTransactionSuccessful();
//		if(writable == null || !writable.isOpen()){
//			writable.setTransactionSuccessful();
//			writable.endTransaction();
//		}
	}
	
	public void endTransaction(){
		connection.getWritableDatabase().endTransaction();
//		if(writable == null || !writable.isOpen()){
//			writable.endTransaction();
//		}
	}
	
	public SQLiteDatabase getWritableDatabase(){
		return connection.getWritableDatabase();
	}
	
	public SQLiteDatabase getReadableDatabase(){
		return connection.getReadableDatabase();
	}
	
	public void close(){
//		connection.close();
	}
}

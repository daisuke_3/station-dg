package com.cyan2.android.app.stationdg.db.dao;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cyan2.android.app.stationdg.db.Columns;
import com.cyan2.android.app.stationdg.db.DBManager;
import com.cyan2.android.app.stationdg.db.Tables;
import com.cyan2.android.app.stationdg.domain.DiagramDetail;

public class DiagramDetailDao extends AbstractDao{

	public DiagramDetailDao(SQLiteDatabase db) {
		super(db);
	}

	@Override
	protected String getTableName() {
		return Tables.DIAGRAM_DETAILS;
	}
	
	public void insert(DiagramDetail diagramDetail){
		insert(createContentValues(diagramDetail));
	}
	
	public void delete(String stationCode,String lineCode){
		SQLiteDatabase db = getDb();;
		db.execSQL("delete from diagram_details where exists(select 1 from diagrams where diagram_details.diagram_id = _id and station_code = ? and line_code = ?)", new String[]{stationCode,lineCode});
	}
	
	public boolean exists(int diagramId){
		SQLiteDatabase db = getDb();;

		Cursor c = db.query(getTableName(), new String[]{Columns.ID}, Columns.DIAGRAM_ID + "=?", new String[]{String.valueOf(diagramId)}, null, null, null);
		boolean exists = false;
		if(c.moveToFirst()){
			exists = true;
		}
		c.close();
		return exists;
	}
	
	public int getMaxId(int diagramId){
		SQLiteDatabase db = getDb();;
		
		Cursor c = db.rawQuery("SELECT max(_id) as _id from diagram_details where diagram_id = ?", new String[]{String.valueOf(diagramId)});
		int id = -1;
		if(c.moveToFirst()){
			id = c.getInt(c.getColumnIndex(Columns.ID));
		}
		c.close();
		return id;
	}
	
	public int getMinId(int diagramId){
		SQLiteDatabase db = getDb();;
		
		Cursor c = db.rawQuery("SELECT min(_id) as _id from diagram_details where diagram_id = ?", new String[]{String.valueOf(diagramId)});
		int id = -1;
		if(c.moveToFirst()){
			id = c.getInt(c.getColumnIndex(Columns.ID));
		}
		c.close();
		return id;
	}
	
	public List<DiagramDetail> find(int diagramId,int limit){
		SQLiteDatabase db = getDb();

		Cursor c = db.rawQuery("select dd.* from diagram_details dd inner join diagrams d on dd.diagram_id = d._id where d._id = ? order by departure_time limit ?", new String[]{String.valueOf(diagramId),String.valueOf(limit)});
			
		List<DiagramDetail> diagramDetails = new ArrayList<DiagramDetail>();
		if(c.moveToFirst()){
			do {
				DiagramDetail d = new DiagramDetail();
				d.setDiagramId(diagramId);
				d.setDiagramDetailId(c.getInt(c.getColumnIndex(Columns.ID)));
				d.setDepartureTime(c.getString(c.getColumnIndex(Columns.DEPARTURE_TIME)));
				d.setKindId(c.getString(c.getColumnIndex(Columns.KIND_ID)));
				d.setKindName(c.getString(c.getColumnIndex(Columns.KIND_NAME)));
				d.setDestination(c.getString(c.getColumnIndex(Columns.DESTINATION)));

				diagramDetails.add(d);
			} while (c.moveToNext());
		}
		c.close();
		
		return diagramDetails;
	}
	
	public List<DiagramDetail> find(int diagramId,Time departureTime,int limit){
		SQLiteDatabase db = getDb();

		Cursor c = db.rawQuery("select dd.* from diagram_details dd inner join diagrams d on dd.diagram_id = d._id where d._id = ? and dd.departure_time > ? order by departure_time limit ?", new String[]{String.valueOf(diagramId),departureTime.toString(),String.valueOf(limit)});
		
		List<DiagramDetail> diagramDetails = new ArrayList<DiagramDetail>();
		if(c.moveToFirst()){
			do {
				DiagramDetail d = new DiagramDetail();
				d.setDiagramId(diagramId);
				d.setDiagramDetailId(c.getInt(c.getColumnIndex(Columns.ID)));
				d.setDepartureTime(c.getString(c.getColumnIndex(Columns.DEPARTURE_TIME)));
				d.setKindId(c.getString(c.getColumnIndex(Columns.KIND_ID)));
				d.setKindName(c.getString(c.getColumnIndex(Columns.KIND_NAME)));
				d.setDestination(c.getString(c.getColumnIndex(Columns.DESTINATION)));

				diagramDetails.add(d);
			} while (c.moveToNext());
		}
		c.close();
		
		return diagramDetails;
	}
	
	public List<DiagramDetail> findUpperDiagramId(int diagramId,int diagramDetailId,String departureTime,int limit){
		SQLiteDatabase db = getDb();
		
		Cursor c = db.rawQuery("select dd.* from diagram_details dd inner join diagrams d on dd.diagram_id = d._id where d._id = ? and dd.departure_time > ? and dd._id > ? order by departure_time limit ?", new String[]{String.valueOf(diagramId),departureTime,String.valueOf(diagramDetailId),String.valueOf(limit)});
		
		List<DiagramDetail> diagramDetails = new ArrayList<DiagramDetail>();
		if(c.moveToFirst()){
			do {
				DiagramDetail d = new DiagramDetail();
				d.setDiagramId(diagramId);
				d.setDiagramDetailId(c.getInt(c.getColumnIndex(Columns.ID)));
				d.setDepartureTime(c.getString(c.getColumnIndex(Columns.DEPARTURE_TIME)));
				d.setKindId(c.getString(c.getColumnIndex(Columns.KIND_ID)));
				d.setKindName(c.getString(c.getColumnIndex(Columns.KIND_NAME)));
				d.setDestination(c.getString(c.getColumnIndex(Columns.DESTINATION)));

				diagramDetails.add(d);
			} while (c.moveToNext());
		}
		c.close();
			
		return diagramDetails;
	}
	
	private ContentValues createContentValues(DiagramDetail diagramDetail){
		ContentValues values = new ContentValues();
		values.put(Columns.DIAGRAM_ID, diagramDetail.getDiagramId());
		values.put(Columns.DEPARTURE_TIME, diagramDetail.getDepartureTime().toString());
		values.put(Columns.KIND_ID, diagramDetail.getKindId());
		values.put(Columns.KIND_NAME, diagramDetail.getKindName());
		values.put(Columns.DESTINATION, diagramDetail.getDestination());

		return values;
	}
}

package com.cyan2.android.app.stationdg.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cyan2.android.app.stationdg.db.Columns;
import com.cyan2.android.app.stationdg.db.DBManager;
import com.cyan2.android.app.stationdg.db.Tables;
import com.cyan2.android.app.stationdg.domain.Line;

public class LineDao extends AbstractDao{

	public LineDao(SQLiteDatabase db) {
		super(db);
	}

	@Override
	protected String getTableName() {
		return Tables.LINES;
	}
	
	public void insert(Line line){
		if(exists(line.getLineCode())){
			return;
		}
		insert(createContentValues(line));
	}
	
	public boolean exists(String lineCode){
		SQLiteDatabase db = getDb();
		Cursor c = db.query(getTableName(), new String[]{Columns.ID}, Columns.LINE_CODE + "=?", new String[]{lineCode}, null, null, null);
		boolean exists = false;
		if(c.moveToFirst()){
			exists = true;
		}
		c.close();
		return exists;
	}
	private ContentValues createContentValues(Line line){
		ContentValues values = new ContentValues();
		values.put(Columns.LINE_CODE, line.getLineCode());
		values.put(Columns.LINE_NAME, line.getLineName());

		return values;
	}
}

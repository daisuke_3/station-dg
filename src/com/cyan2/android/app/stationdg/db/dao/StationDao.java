package com.cyan2.android.app.stationdg.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cyan2.android.app.stationdg.db.Columns;
import com.cyan2.android.app.stationdg.db.DBManager;
import com.cyan2.android.app.stationdg.db.Tables;
import com.cyan2.android.app.stationdg.domain.Station;

public class StationDao extends AbstractDao{

	public StationDao(SQLiteDatabase db) {
		super(db);
	}

	@Override
	protected String getTableName() {
		return Tables.STATIONS;
	}
	
	public void insert(Station station){
		if(exists(station.getStationCode())){
			return;
		}
		insert(createContentValues(station));
	}
	
	public boolean exists(String stationCode){
		SQLiteDatabase db = getDb();
		Cursor c = db.query(getTableName(), new String[]{Columns.ID}, Columns.STATION_CODE + "=?", new String[]{stationCode}, null, null, null);
		boolean exists = false;
		if(c.moveToFirst()){
			exists = true;
		}
		return exists;
	}
	private ContentValues createContentValues(Station station){
		ContentValues values = new ContentValues();
		values.put(Columns.STATION_CODE, station.getStationCode());
		values.put(Columns.STATION_NAME, station.getStationName());

		return values;
	}
}

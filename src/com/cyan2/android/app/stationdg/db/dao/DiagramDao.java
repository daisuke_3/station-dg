package com.cyan2.android.app.stationdg.db.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan2.android.app.stationdg.db.Columns;
import com.cyan2.android.app.stationdg.db.DBManager;
import com.cyan2.android.app.stationdg.db.Tables;
import com.cyan2.android.app.stationdg.domain.DayType;
import com.cyan2.android.app.stationdg.domain.Diagram;

public class DiagramDao extends AbstractDao{
		
	public DiagramDao(SQLiteDatabase db) {
		super(db);
	}
	
	@Override
	protected String getTableName() {
		return Tables.DIAGRAMS;
	}

	public void insert(Diagram diagram){
		long key = exists(diagram.getStationCode(), diagram.getLineCode(),diagram.getDistrictName(),diagram.getDayType());
		if(key == -1){
			key = insert(createContentValues(diagram));
			Log.d("station-dg", "newly key:" + key);
		}
		diagram.setDiagramId((int)key);
	}
	
	public void update(Diagram diagram){
		update(createContentValues(diagram), diagram.getDiagramId());
	}
	
	public void delete(Diagram diagram){
		delete(diagram.getDiagramId());
	}
	
	public void delete(String stationCode,String lineCode){
		SQLiteDatabase db = getDb();
		db.execSQL("delete from diagrams where station_code = ? and line_code = ?", new String[]{stationCode,lineCode});
	}
	
	public long exists(String stationCode,String lineCode,String districtName,DayType dayTyte){
		
		SQLiteDatabase db = getDb();;

		Cursor c = db.query(getTableName(), new String[]{Columns.ID}, Columns.STATION_CODE + "=? and " + Columns.LINE_CODE + "=? and " + Columns.DISTRICT_NAME + "=? and " + Columns.DAY_TYPE + "=?", new String[]{stationCode,lineCode,districtName,dayTyte.getCode()}, null, null, null);
		long key = -1;
		if(c.moveToFirst()){
			key = c.getLong(c.getColumnIndex(Columns.ID));
		}
		c.close();
		return key;
	}
	
	public int findId(String stationCode,String lineCode,String districtName,DayType dayType){
		SQLiteDatabase db = getDb();;

		String where = "station_code = ? and line_code = ? and district_name = ? and day_type = ?";
		Cursor c = db.query(getTableName(), new String[]{Columns.ID}, where, new String[]{stationCode,lineCode,districtName,dayType.getCode()}, null, null, null);
		int key = -1;
		if(c.moveToFirst()){
			key = c.getInt(c.getColumnIndex(Columns.ID));
		}
		c.close();
		return key;
	}
	
	public List<Diagram> findAll(){
		SQLiteDatabase db = getDb();;

		Cursor c = db.rawQuery("SELECT d.station_code as station_code,s.station_name as station_name,d.line_code as line_code,l.line_name as line_name,d.district_name as district_name FROM diagrams d inner join stations s on d.station_code = s.station_code inner join lines l on d.line_code = l.line_code group by s.station_name,l.line_name,d.district_name order by s.station_name,l.line_name,d.district_name",null);

		List<Diagram> list = new ArrayList<Diagram>();
		if(c.moveToFirst()){
			do {
				Diagram d = new Diagram();
				d.setStationCode(c.getString(c.getColumnIndex(Columns.STATION_CODE)));
				d.setStationName(c.getString(c.getColumnIndex(Columns.STATION_NAME)));
				d.setLineCode(c.getString(c.getColumnIndex(Columns.LINE_CODE)));
				d.setLineName(c.getString(c.getColumnIndex(Columns.LINE_NAME)));
				d.setDistrictName(c.getString(c.getColumnIndex(Columns.DISTRICT_NAME)));
				
				list.add(d);
			} while (c.moveToNext());
		}
		c.close();
		return list;
	}
	
	private ContentValues createContentValues(Diagram diagram){
		ContentValues values = new ContentValues();
		values.put(Columns.STATION_CODE, diagram.getStationCode());
		values.put(Columns.LINE_CODE, diagram.getLineCode());
		values.put(Columns.DISTRICT_NAME, diagram.getDistrictName());
		values.put(Columns.DAY_TYPE, diagram.getDayType().getCode());

		return values;
	}
}

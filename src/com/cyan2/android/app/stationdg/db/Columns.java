package com.cyan2.android.app.stationdg.db;

public final class Columns {
	public static final String ID = "_id";
	
	public static final String DIAGRAM_ID = "diagram_id";
	public static final String STATION_CODE = "station_code";
	public static final String STATION_NAME = "station_name";
	public static final String LINE_CODE = "line_code";
	public static final String LINE_NAME = "line_name";
	public static final String DAY_TYPE = "day_type";
	public static final String DISTRICT_NAME = "district_name";
	public static final String DEPARTURE_TIME = "departure_time";
	public static final String KIND_ID = "kind_id";
	public static final String KIND_NAME = "kind_name";
	public static final String DESTINATION = "destination";
}
